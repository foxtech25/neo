### How install: ###
* git clone
* cd laradock
* docker-compose build redis mysql nginx php-fpm
* docker-compose up -d redis mysql nginx
* docker-compose exec workspace bash
* composer install
* php artisan migrate
* cp .env.example .env 


### Save asteroid values in DB: ### 
**php artisan neo:info**


### Routes: ###
* GET /hazardous
* GET /fastest
* GET /best-year
* GET /best-month


### Response: ### on any route format JSON