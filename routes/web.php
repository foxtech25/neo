<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return [
        'hello' => 'world'
    ];
});

Route::get('hazardous', 'NeoController@isHazardous');

Route::get('fastest', 'NeoController@fastest');

Route::get('best-year', 'NeoController@bestYear');

Route::get('best-month', 'NeoController@bestMonth');