<?php

return [
    'neo_api_url' => env('NEO_API_URL'),

    'neo_api_key' => env('NEO_API_KEY')
];
