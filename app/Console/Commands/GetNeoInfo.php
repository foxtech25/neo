<?php

namespace App\Console\Commands;

use App\Adapter\NeoAdapter;
use App\Neo;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class GetNeoInfo
 * @package App\Console\Commands
 */
class GetNeoInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'neo:info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Neo Info';

    /**
     * @var NeoAdapter
     */
    private $adapter;

    /**
     * GetNeoInfo constructor.
     * @param NeoAdapter $adapter
     */
    public function __construct(NeoAdapter $adapter)
    {
        $this->adapter = $adapter;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Running...\n";

        $neos = $this->adapter->findNeoByDate(
            (new Carbon('3 days ago'))->toDateString(),
            (new Carbon())->toDateString()
        );

        foreach (json_decode($neos)->near_earth_objects as $date => $neo) {
            foreach ($neo as $neoForDb) {
                try {
                    Neo::create([
                        'date'         => $date,
                        'reference'    => $neoForDb->neo_reference_id,
                        'name'         => $neoForDb->name,
                        'speed'        => $neoForDb->close_approach_data[0]
                            ->relative_velocity
                            ->kilometers_per_hour,
                        'is_hazardous' => $neoForDb->is_potentially_hazardous_asteroid
                    ]);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }

        echo "Done.\n";
    }
}
