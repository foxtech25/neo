<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Neo
 * @package App
 */
class Neo extends Model
{
    /**
     * @var string
     */
    protected $table = 'neos';

    /**
     * @var array
     */
    protected $fillable = [
        'date',
        'reference',
        'name',
        'speed',
        'is_hazardous'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;
}
