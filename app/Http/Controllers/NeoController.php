<?php

namespace App\Http\Controllers;

use App\Neo;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class NeoController
 * @package App\Http\Controllers
 */
class NeoController extends Controller
{
    /**
     * @return mixed
     */
    public function isHazardous()
    {
        return Neo::where('is_hazardous', true)->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function fastest(Request $request)
    {
        $hazardous = $request->input('hazardous') === 'true' ? true : false;

        return Neo::where('is_hazardous', $hazardous)->orderBy('speed', 'desc')
            ->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function bestYear(Request $request)
    {
        $hazardous = $request->input('hazardous') === 'true' ? true : false;

        $dates = Neo::select('date')->groupBy('date')->get();
        $response = [];

        foreach ($dates as $date) {
            $year = Carbon::parse($date->date)->year;
            $asteroids = Neo::where('date', 'like', $year . '%')
                ->where('is_hazardous', $hazardous)
                ->get();
            $count = $asteroids->count();

            if ($count) {
                $response[$year] = [
                    'count' => $count,
                    'ateroids' => $asteroids
                ];
            }
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function bestMonth(Request $request)
    {
        $hazardous = $request->input('hazardous') === 'true' ? true : false;

        $dates = Neo::select('date')->groupBy('date')->get();
        $response = [];

        foreach ($dates as $date) {
            $month = Carbon::parse($date->date)->format('m');
            $asteroids = Neo::where('date', 'like', '%-' . $month . '-%')
                ->where('is_hazardous', $hazardous)
                ->get();
            $count = $asteroids->count();

            if ($count) {
                $response[$month] = [
                    'count' => $count,
                    'ateroids' => $asteroids
                ];
            }
        }

        return $response;
    }
}
