<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 13.07.17
 * Time: 22:40
 */

namespace App\Adapter;

use GuzzleHttp\Client;

/**
 * Class NeoAdapter
 * @package App\Adapter
 */
class NeoAdapter
{
    /**
     * @var mixed
     */
    private $apiUrl;

    /**
     * @var mixed
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $client;

    /**
     * NeoAdapter constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->apiUrl = config('neo.neo_api_url');
        $this->apiKey = config('neo.neo_api_key');
        $this->client = $client;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @param bool $detailed
     * @return string
     */
    public function findNeoByDate(
        string $startDate,
        string $endDate,
        bool $detailed = false
    ): string {
        return $this->client->get(
            $this->apiUrl . 'feed', [
            'query' => [
                'api_key'    => $this->apiKey,
                'start_date' => $startDate,
                'end_date'   => $endDate,
                'detailed'   => $detailed
            ]
        ])
            ->getBody()
            ->getContents();
    }
}